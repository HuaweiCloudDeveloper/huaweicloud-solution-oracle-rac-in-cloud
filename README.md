[TOC]

**解决方案介绍**
===============
该解决方案可以帮助用户在华为云上一键部署Oracle RAC基础环境，助力企业快速完成数字化转型，为企业提供一个云上高可靠、高性能的核心数据库业务架构。适用于如下场景：

- 企业快速、低成本开展业务，同时又有高性能和高可靠要求场景。

- 使用Oracle数据库的业务系统迁移上云场景。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/oracle-rac-in-cloud.html


**架构图**
---------------
![方案架构](./document/oracle-rac-in-cloud.png)

**架构描述**
---------------
该解决方案会部署如下资源：
1. 创建云服务器（弹性云服务器部署或裸金属服务器）
2. 安全组可以保护弹性云服务器的网络安全，通过配置安全组规则，限定云服务器的访问端口。
3. 弹性公网IP并绑定云服务，用于创建资源时的环境部署。
4. 云硬盘6块，用于数据存储。

**组织结构**
---------------

``` lua
huaweicloud-solution-oracle-rac-in-cloud
├── oracle-rac-in-cloud-ecs.tf.json -- 弹性云服务器资源编排模板
├── oracle-rac-in-cloud-ecs-new-vpc.tf.json -- 弹性云服务器新建VPC资源编排模板
├── oracle-rac-in-cloud-bms.tf.json.tf.json -- 裸金属服务器资源编排模板
    ├── oracle_init.sh  -- 脚本配置文件
```
**开始使用**
---------------
**安全组规则修改（可选）**

安全组实际是网络流量访问策略，包括网络流量入方向规则和出方向规则，通过这些规则为安全组内具有相同保护需求并且相互信任的云服务器、云容器、云数据库等实例提供安全保护。
如果您的实例关联的安全组策略无法满足使用需求，比如需要添加、修改、删除某个TCP端口，请参考以下内容进行修改。
1. 添加安全组规则：
根据业务使用需求需要新开放某个TCP端口，请参考添加安全组规则添加入方向规则，打开指定的TCP端口。
2. 修改安全组规则：
安全组规则设置不当会造成严重的安全隐患。您可以参考修改安全组规则，来修改安全组中不合理的规则，保证云服务器等实例的网络安全。
3. 删除安全组规则：当安全组规则入方向、出方向源地址/目的地址有变化时，或者不需要开放某个端口时，您可以参考删除安全组规则进行安全组规则删除。

**清理临时资源（可选）**

1.在[弹性公网EIP控制台](https://console.huaweicloud.com/vpc/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/eip/eips/list)，解绑该方案创建的EIP。

![解绑EIP](./document/readme-image-001.png)

2.解绑后，在该方案创建的EIP后单击“释放”，即可清除临时EIP资源。

![释放EIP](./document/readme-image-002.png)

**查看部署资源**

1、登录[华为云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，选择“北京四”区域。
![华为云控制台](./document/readme-image-003.png)

2、点击ECS界面，可以看到我们新创建出来的ECS。。

![服务器](./document/readme-image-004.png)

3、点击EVS，可以看到我们已购买的6块共享磁盘。

![EVS](./document/readme-image-005.png)
